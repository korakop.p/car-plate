import { authorize } from 'react-native-app-auth';
import Config from 'react-native-config'

// config = {
//     type: 'authorization_code'
//     issuer: '',
//     clientId: '',
//     redirectUrl: '',
//     additionalParameters: {},
//     scopes: []
// };

// config = {
//     type: 'resource_owner'
//     clientId: '',
//     clientSecret: '',
//     username: '',
//     password: ''
// };

// config = {
//     type: 'social_authentication'
//     issuer: '',
//     clientId: '',
//     redirectUrl: '',
//     additionalParameters: {},
//     scopes: [],
//     idpTd:''
// };

const refreshTokenUrl = Config.OPENID_ISSUER+"/v1/token"
const revokeTokenUrl = Config.OPENID_ISSUER+"/v1/revoke"
const getUserInfoUrl = Config.OPENID_ISSUER+"/v1/userinfo"

const authorization = async (config) => {
    if(config.type == 'authorization_code'){
        try {
            const authResponse = await authorize(config)
            return authResponse
        } catch (error) {
            return {error_code:'20002', error_desc:error.message}
        }
    }
    else if(config.type == 'resource_owner'){
        return {error_code:'20003', error_desc:'resource_owner not support'}
    }
    else if(config.type == 'social_authentication'){
        return {error_code:'20004', error_desc:'social_authentication not support'}
    }
    else{
        return {error_code:'20001', error_desc:'type not support'}
    }
}

// config = {
//     clientId: ''
//     refreshToken: ''
// };

const refreshToken = async (config) => {

    try {
        let response = await fetch(
            refreshTokenUrl, {
            method: 'POST',
            headers: { 
                "Content-Type": 'application/x-www-form-urlencoded',
                "accept": 'application/json',
            },
            body: "grant_type=refresh_token&refresh_token="+config.refreshToken+"&client_id="+config.clientId
        });
        if(response.status == 200){
            let refreshResponse = await response.json();
            if ('error' in refreshResponse){
                return {error_code:'20005', error_desc:refreshResponse.error_description}
            }
            else{
                return {
                    accessToken: refreshResponse.access_token,
                    accessTokenExpirationDate: refreshResponse.expires_in,
                    refreshToken: refreshResponse.refresh_token,
                    idToken: refreshResponse.id_token,
                    tokenType: refreshResponse.token_type
                }
            }
        }
        else{

        }
      } 
      catch (error) {
        console.error(error);
      }
}

// config = {
//     clientId: ''
//     token: ''
// };

const revokeToken = async (config) => {

    try {
        let response = await fetch(
            revokeTokenUrl, {
            method: 'POST',
            headers: { 
                "Content-Type": 'application/x-www-form-urlencoded',
                "accept": 'application/json',
            },
            body: "token="+config.token+"&client_id="+config.clientId
        });
        console.log(response) 
        if(response.status == 200){
            return {}
        }
        else{
            return {error_code:'20005', error_desc: 'Revoke Token Error'}
        }
      } catch (error) {
        console.error(error);
        return {error_code:'20005', error_desc: 'Revoke Token Error'}
      }
}

// config = {
//     access_token: ''
// };

const getUserInfo = async (config) => {

    try {
        let response = await fetch(
            getUserInfoUrl, {
            method: 'GET',
            headers: { 
                "Content-Type": 'application/x-www-form-urlencoded',
                "accept": 'application/json',
                "authorization" : 'Bearer '+config.access_token
            }        
        });
        
        if(response.status == 200){
            let getUserInfoResponse = await response.json();
            console.log(getUserInfoResponse)
            const resp_data = {
                email: getUserInfoResponse.email,
                erm_role: getUserInfoResponse.erm_role,
                family_name: getUserInfoResponse.family_name,
                given_name: getUserInfoResponse.given_name,
                username: getUserInfoResponse.preferred_username,
                userid: getUserInfoResponse.sub
            }
            return resp_data
        }
        else{
            return {error_code:'20005', error_desc: 'Get User Info Error'}
        }
      } catch (error) {
        console.error(error);
        return {error_code:'20005', error_desc: 'Get User Info Error'}
    }
}

export const Authentication = {
    authorization:(param)=> authorization(param),
    refreshToken:(param)=> refreshToken(param),
    revokeToken:(param)=> revokeToken(param),
    getUserInfo:(param)=> getUserInfo(param)
}