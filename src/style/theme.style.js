import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  containerTheme: {
    flex: 1
  },
  containerCenterRow:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  containerCenterColumn:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  containerMarginTop:{
    marginTop: '15%',
  },
  historyBG:{
    flex: 1,
    backgroundColor:'#F7FAFD',
  }
});
