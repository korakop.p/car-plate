import React, { Component } from 'react'
import {View,Text,SafeAreaView} from 'react-native'
import styles from '../style/theme.style'
import LinearGradient from 'react-native-linear-gradient';
import HistoryNew from '../components/History_New/HistoryNew.component'

class HistoryNewScreen extends Component {
    
    render(){
        return(
            <SafeAreaView style={styles.container}>
                    <View style={[styles.historyBG]}>
                        <HistoryNew/>
                    </View>
            </SafeAreaView>
        )
    }
}

export default HistoryNewScreen
