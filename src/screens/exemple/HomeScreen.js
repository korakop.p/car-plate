import React, { Component } from 'react'
import {View,Text,StyleSheet,TouchableOpacity} from 'react-native'
import Config from 'react-native-config'


class HomeScreen extends Component {

    goToAuthen=()=>{
        console.log('hoem :',this.props)
        this.props.navigation.navigate('AuthenScreen')
    }

    goToStorage=()=>{
        console.log('hoem :',this.props)
        this.props.navigation.navigate('StorageScreen')
    }

    goToSecureStorage=()=>{
        console.log('hoem :',this.props)
        this.props.navigation.navigate('SecureStorageScreen')
    }

    goToRedux=()=>{
        console.log('hoem :',this.props)
        this.props.navigation.navigate('ReduxScreen')
    }
    
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.welcome}>Welcome to</Text>
                <Text style={styles.welcome}>{Config.APPLICATION_NAME}</Text>
                <Text style={styles.welcome}>Version : {Config.APPLICATION_VERSION}</Text>

                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={this.goToAuthen}
                    underlayColor='#fff'>
                        <Text style={styles.loginText}>Test Authentication</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={this.goToStorage}
                    underlayColor='#fff'>
                        <Text style={styles.loginText}>Test Storage</Text>
                </TouchableOpacity> 

                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={this.goToSecureStorage}
                    underlayColor='#fff'>
                        <Text style={styles.loginText}>Test Secure Storage</Text>
                </TouchableOpacity> 
                
                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={this.goToRedux}
                    underlayColor='#fff'>
                        <Text style={styles.loginText}>Test Redux</Text>
                </TouchableOpacity> 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    loginScreenButton:{
      marginRight:40,
      marginLeft:40,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#017CC0',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});

export default HomeScreen
