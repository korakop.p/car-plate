import React, { Component } from 'react'
import {View,Text,StyleSheet,TouchableOpacity,TextInput} from 'react-native'
import SecureStorage, { ACCESS_CONTROL, ACCESSIBLE, AUTHENTICATION_TYPE } from 'react-native-secure-storage'

const config = {
    accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
    accessible: ACCESSIBLE.WHEN_UNLOCKED,
    authenticationPrompt: 'auth with yourself',
    service: 'example',
    authenticateType: AUTHENTICATION_TYPE.BIOMETRICS
}

class SecureStorageScreen extends Component {

    constructor(props) {
        super(props);
        this.state = { value:''};
        SecureStorage.setItem('test_secure_key', 'test_secure_value', config)
    }

    show = async () => {
        const get_value = await SecureStorage.getItem('test_secure_key',config);
        this.setState({value:get_value})
    }
   
    render(){
        
        return(
            <View style={styles.container}>
                <Text>Key :: test_secure_key  Value :: {this.state.value}</Text>
                <TouchableOpacity
                            style={styles.loginScreenButton}
                            onPress={this.show}
                            underlayColor='#fff'>
                                <Text style={styles.loginText}>Render</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    container2: {
        backgroundColor: '#F5FCFF',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:40,
        borderColor : "gray",
        borderRadius: 4,
        borderWidth: 0.5,
        paddingTop:30,
        paddingBottom:30,
        paddingLeft:30,
        paddingRight:30,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    loginScreenButton:{
      marginRight:40,
      marginLeft:40,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#017CC0',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});

export default SecureStorageScreen
