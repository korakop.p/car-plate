import React, { Component } from 'react'
import {View,Text,StyleSheet,TouchableOpacity,TextInput} from 'react-native'
import {connect} from 'react-redux'
import {renderName,renderLastname} from '../../redux/actions/index'
class ReduxScreen extends Component {

    onClickSetName = ()=>{
        this.props.setName();
    }

    onClickSetLastname = ()=>{
        this.props.setLastname();
    }

    render(){
        return(
            <View style={styles.container}>
                <Text>Name :: {this.props.name}  LastName :: {this.props.lastname}</Text>
                <TouchableOpacity
                            style={styles.loginScreenButton}
                            onPress={this.onClickSetName}
                            underlayColor='#fff'>
                                <Text style={styles.loginText}>Render Name</Text>
                </TouchableOpacity>

                <TouchableOpacity
                            style={styles.loginScreenButton}
                            onPress={this.onClickSetLastname}
                            underlayColor='#fff'>
                                <Text style={styles.loginText}>Render Lastname</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    container2: {
        backgroundColor: '#F5FCFF',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:40,
        borderColor : "gray",
        borderRadius: 4,
        borderWidth: 0.5,
        paddingTop:30,
        paddingBottom:30,
        paddingLeft:30,
        paddingRight:30,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    loginScreenButton:{
      marginRight:40,
      marginLeft:40,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#017CC0',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});
const mapStateToProps = stateRedux =>{
    console.log("asdasd :: ", stateRedux)
    return {
        name:stateRedux.RENDER.name,
        lastname:stateRedux.RENDER.lastname
}
}

const mapDispatchToProps = (dispatch) => ({
      setName:()=>{
            dispatch(renderName());
      },
      setLastname:()=>{
        dispatch(renderLastname("Witherspoon"));
      }
  });

export default connect(mapStateToProps,mapDispatchToProps)(ReduxScreen); 