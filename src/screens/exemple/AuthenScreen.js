/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity,Alert,LayoutAnimation} from 'react-native';
import Config from 'react-native-config'
import { Authentication } from '../../platform_modules/Authentication/Authentication';

type State = {
  hasLoggedInOnce: boolean,
  accessToken: ?string,
  accessTokenExpirationDate: ?string,
  refreshToken: ?string,
  idToken: ?string
};

const authorizationCodeConfig = {
  issuer: Config.OPENID_ISSUER,
  clientId: Config.OPENID_CLIENT_ID,
  redirectUrl: Config.OPENID_REDIRECT_URL,
  scopes: Config.OPENID_SCOPE.split(','),
  type: 'authorization_code'
};

export default class AuthenScreen extends Component<{}, State> {

  state = {
    hasLoggedInOnce: false,
    accessToken: '',
    accessTokenExpirationDate: '',
    refreshToken: '',
    idToken: ''
  };

  animateState(nextState: $Shape<State>, delay: number = 0) {
    setTimeout(() => {
      this.setState(() => {
        LayoutAnimation.easeInEaseOut();
        return nextState;
      });
    }, delay);
  }

  app_authorize = async () => {
    try {
      const authorizationReponse = await Authentication.authorization(authorizationCodeConfig);
      //check error in authorizationReponse
      console.log(authorizationReponse)
      this.animateState(
        {
          hasLoggedInOnce: true,
          accessToken: authorizationReponse.accessToken,
          accessTokenExpirationDate: authorizationReponse.accessTokenExpirationDate,
          refreshToken: authorizationReponse.refreshToken,
          idToken: authorizationReponse.idToken
        },
        500
      );
    } catch (error) {
      Alert.alert('Failed to log in', error.message);
    }
  };

  view_access_token = async () => {Alert.alert('Access Token', this.state.accessToken);}
  view_refresh_token = async () => {Alert.alert('Refresh Token', this.state.refreshToken);}
  view_expiration = async () => {Alert.alert('Expiration', this.state.accessTokenExpirationDate);}
  view_id_token = async () => {Alert.alert('Id Token', this.state.idToken);}

  token_refresh = async () => {
    try {
      const refreshTokenResponse = await Authentication.refreshToken({clientId:Config.OPENID_CLIENT_ID,refreshToken:this.state.refreshToken});
      console.log(refreshTokenResponse);
      this.animateState(
        {
          hasLoggedInOnce: true,
          accessToken: refreshTokenResponse.accessToken,
          accessTokenExpirationDate: refreshTokenResponse.accessTokenExpirationDate,
          refreshToken: refreshTokenResponse.refreshToken,
          idToken: refreshTokenResponse.idToken
        },
        500
      );
    } catch (error) {
      Alert.alert('Failed', error.message);
    }
  };

  access_token_revoke = async () => {
    try {
      const revokeTokenResponse = await Authentication.revokeToken({clientId:Config.OPENID_CLIENT_ID,token:this.state.accessToken});
      console.log(revokeTokenResponse);
      // this.animateState(
      //   {
      //     hasLoggedInOnce: true,
      //     accessToken: revokeTokenResponse.accessToken,
      //     accessTokenExpirationDate: revokeTokenResponse.accessTokenExpirationDate,
      //     refreshToken: revokeTokenResponse.refreshToken,
      //     idToken: revokeTokenResponse.idToken
      //   },
      //   500
      // );
    } catch (error) {
      Alert.alert('Failed', error.message);
    }
  };

  refresh_token_revoke = async () => {
    try {
      const revokeTokenResponse = await Authentication.revokeToken({clientId:Config.OPENID_CLIENT_ID,token:this.state.refreshToken});
      console.log(revokeTokenResponse);
      // this.animateState(
      //   {
      //     hasLoggedInOnce: true,
      //     accessToken: revokeTokenResponse.accessToken,
      //     accessTokenExpirationDate: revokeTokenResponse.accessTokenExpirationDate,
      //     refreshToken: revokeTokenResponse.refreshToken,
      //     idToken: revokeTokenResponse.idToken
      //   },
      //   500
      // );
    } catch (error) {
      Alert.alert('Failed', error.message);
    }
  };

  get_user_info = async () => {
    try {
      const getUserInfoResponse = await Authentication.getUserInfo({access_token:this.state.accessToken});
      Alert.alert('User Info', JSON.stringify(getUserInfoResponse));

    } catch (error) {
      Alert.alert('Failed', error.message);
    }
  };

  render() {
    const {state} = this;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Test Authentication</Text>
        {!state.accessToken && (
          <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={this.app_authorize}
          underlayColor='#fff'>
            <Text style={styles.loginText}>Authorize</Text>
          </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={this.view_access_token}
          underlayColor='#fff'>
            <Text style={styles.loginText}>View Access Token</Text>
          </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.loginScreenButton}
            onPress={this.view_refresh_token}
            underlayColor='#fff'>
              <Text style={styles.loginText}>View Refresh Token</Text>
            </TouchableOpacity>

        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.loginScreenButton}
            onPress={this.view_expiration}
            underlayColor='#fff'>
              <Text style={styles.loginText}>View Expiration</Text>
            </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.loginScreenButton}
            onPress={this.view_id_token}
            underlayColor='#fff'>
              <Text style={styles.loginText}>View Id Token</Text>
            </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.refreshScreenButton}
            onPress={this.get_user_info}
            underlayColor='#fff'>
              <Text style={styles.loginText}>get User Info</Text>
            </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.refreshScreenButton}
            onPress={this.token_refresh}
            underlayColor='#fff'>
              <Text style={styles.loginText}>Refresh</Text>
            </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.revokeScreenButton}
            onPress={this.access_token_revoke}
            underlayColor='#fff'>
              <Text style={styles.loginText}>Revoke Access Token</Text>
            </TouchableOpacity>
        )}

        {!!state.accessToken && (
          <TouchableOpacity
            style={styles.revokeScreenButton}
            onPress={this.refresh_token_revoke}
            underlayColor='#fff'>
              <Text style={styles.loginText}>Revoke Refresh Token</Text>
            </TouchableOpacity>
        )}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  loginScreenButton:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#017CC0',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  revokeScreenButton:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#d14251',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  refreshScreenButton:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#42716d',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10
  }
});
