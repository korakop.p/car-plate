import React, { Component } from 'react'
import {View,Text,StyleSheet,TouchableOpacity,TextInput} from 'react-native'
import {AsyncStorage} from 'react-native';

class StorageScreen extends Component {

    constructor(props) {
        super(props);
        this.state = { value:''};
        AsyncStorage.setItem('test_storage_key', 'test_storage_value');
    }

    // save = async () => {
    //     await AsyncStorage.setItem(this.state.key, this.state.value);
    //     this.state.key = ""
    //     this.state.value = ""
    // }

    show = async () => {
        const get_value = await AsyncStorage.getItem('test_storage_key');
        this.setState({value:get_value})
    }
   
    render(){
        
        return(
            <View style={styles.container}>
                <Text>Key :: test_storage_key  Value :: {this.state.value}</Text>
                <TouchableOpacity
                            style={styles.loginScreenButton}
                            onPress={this.show}
                            underlayColor='#fff'>
                                <Text style={styles.loginText}>Render</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    container2: {
        backgroundColor: '#F5FCFF',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:40,
        borderColor : "gray",
        borderRadius: 4,
        borderWidth: 0.5,
        paddingTop:30,
        paddingBottom:30,
        paddingLeft:30,
        paddingRight:30,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    loginScreenButton:{
      marginRight:40,
      marginLeft:40,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#017CC0',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});

export default StorageScreen
