import React, { Component } from 'react'
import {View,Text,StyleSheet,SafeAreaView} from 'react-native'
import styles from '../style/theme.style'
import LinearGradient from 'react-native-linear-gradient';
import Validate from '../components/Validate/Validate.component'

class EditScreen extends Component {
    
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#283D81', '#2074B9','#10ABDB' ]} style={[styles.containerTheme]}>
                    {/* <View style={[styles.container, styles.bgGradient]}>
                        <Text style={styles.welcome}>EditScreen</Text>
                    </View> */}
                    {/* <View style={{height: '5%',backgroundColor:'red'}}></View>

                    <View style={styles.containerCenterRow}>
                            <Validate/>
                    </View>

                    <View style={{height: '10%'}}></View> */}
                    <Validate/>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}

export default EditScreen
