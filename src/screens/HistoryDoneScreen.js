import React, { Component } from 'react'
import {View,Text,SafeAreaView} from 'react-native'
import styles from '../style/theme.style'
import LinearGradient from 'react-native-linear-gradient';
import HistoryDone from '../components/History_Done/HistoryDone.component'

class HistoryDoneScreen extends Component {
    
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#283D81', '#2074B9','#10ABDB' ]} style={[styles.container]}>
                    <View style={[styles.container, styles.bgGradient]}>
                        <HistoryDone/>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}

export default HistoryDoneScreen
