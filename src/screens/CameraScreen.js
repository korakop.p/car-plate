import React, { Component } from 'react'
import {View,Text,StyleSheet,SafeAreaView} from 'react-native'
import styles from '../style/theme.style'
import LinearGradient from 'react-native-linear-gradient';

class CameraScreen extends Component {
    
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#283D81', '#2074B9','#10ABDB' ]} style={[styles.container]}>
                    <View style={[styles.container, styles.bgGradient]}>
                        <Text style={styles.welcome}>Welcome to</Text>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}

export default CameraScreen
