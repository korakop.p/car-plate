import React, {Component} from 'react';
import {View,TouchableOpacity,ScrollView} from 'react-native';
import styles from './HistoryDone.component.style';
import CardList from '../Card_List/CardList.component'

// Icon Usage
// import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
// import icoMoonConfig from '../../assets/selection.json';
// const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class HistoryDone extends Component {

  render () {
    return (
      <View style={styles.container}>
        <ScrollView>
          <TouchableOpacity style={styles.card}>
            <CardList registerPlate='3ฟห1231' province='กรุงเทพหมานคร' dateTime='10:00 น. 23-01-19' status='done'/>
          </TouchableOpacity>
          {[1,1,1,1,1,1,1,11,1,1,1,1,1,1,11,].map((item,index)=>( 
          <TouchableOpacity style={styles.card}>
            <CardList registerPlate='1กย3456' province='กรุงเทพหมานคร' dateTime='10:00 น. 23-01-19' status='done'/>
          </TouchableOpacity>))}
        </ScrollView>
      </View>
    );
  }
} 

export default HistoryDone;
