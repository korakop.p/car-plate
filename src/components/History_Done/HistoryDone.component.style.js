import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  card:{
    paddingLeft:'5%',
    paddingRight: '5%',
    paddingTop:'1.5%',
    paddingBottom:'1.5%'
  }
});
