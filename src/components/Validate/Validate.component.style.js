import {StyleSheet,Platform} from 'react-native';

export default StyleSheet.create({

  container: {
    flex:1
  },
  top:{
    height:'5%'
  },
  bottom:{
    height:'15%',
  },
  bottomContent:{
    flex:1,
    flexDirection: 'column',
    justifyContent:'flex-end',
    alignItems:'center',
  },
  body:{
    flex:1,
    paddingLeft:'5%',
    paddingRight:'5%',
  },
  bodyContent:{
    flex:1,
    backgroundColor:'#fff',
    borderRadius:25,
    shadowColor: "#0E5898",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 11,
  },
  viewImage:{
    flex: 1,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    overflow: 'hidden',
  },
  image:{
    width: '100%',
    height: '100%', 
  },
  contentTxt:{
    marginTop: '5%',
    fontSize: 13,
    color: '#5A5A5A',
  },
  viewContent:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  centerContent:{
    justifyContent:'center',
    alignItems:'center'
  },
  headerText:{
    marginTop: '3%',
    fontSize: 24,
    color: 'black',
  },
  txtbox:{
    height: 40,
    width: 275,
    borderColor: '#E0Efff',
    borderBottomWidth:2,
    color: 'black',
    fontSize: 20,
    fontFamily: Platform.OS === 'ios' ? "TISCO" : "TISCO Bold"
  },
  acceptTxt:{
    height: 40,
    color: '#274387',
    fontSize: 17,
    textAlign: 'center'
  },
  cancelTxt:{
    color: '#274387',
    fontSize: 16,
  },
  acceptBtn:{
    marginTop: '10%',
    padding: 7,
    height: 40,
    width: 150,
    backgroundColor: '#FFFFFF',
    fontSize: 17,
    borderRadius:100,
    borderWidth: 2,
    borderColor: '#274387'
  },
  cancelBtn:{
    fontSize: 17
  }
});
