import React, {Component} from 'react';
import {View, Image,Platform ,TextInput,TouchableOpacity,ImageBackground} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './Validate.component.style';
import TextBold from '../Text_Bold/TextBold.component'
import TextRegular from '../Text_Regular/TextRegular.component'

// Icon Usage
// import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
// import icoMoonConfig from '../../assets/selection.json';
// const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class Validate extends Component {

  render () {
    return (
      <KeyboardAwareScrollView 
        //style={styles.container} 
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={true}
      >
      <View style={styles.container}>
        <View style={styles.top}/>

        <View style={styles.body}>
          <View style={styles.bodyContent}>
              <View style={styles.viewImage}>
                <ImageBackground source={{uri: 'https://www.thai-hotnews.com/tn.php?fn=https://obs.line-scdn.net/0hxgeocCstJ0hYFggFGxhYH2JAJCdrejRLPCB2SwR4eXx9JTAbMHRgJntEcHByIGAWNiVoKXkVPHlyIWcYYHVg/w644'}} style={styles.image}>
                </ImageBackground>
              </View>

              <View style={styles.viewContent}>
                <View style={styles.centerContent}>
                  <View>
                    <TextBold content="ตรวจสอบข้อมูล" style={styles.headerText}/>
                  </View>
                  
                  <View>
                    <TextRegular content="เลขทะเบียน" style={styles.contentTxt}/>
                    <TextInput style={[styles.txtbox, Platform.OS == "ios"?{fontWeight: 'bold'}:{}]}></TextInput>
                  </View>
                  
                  <View>
                    <TextRegular content="จังหวัด" style={styles.contentTxt}/>
                    <TextInput 
                      style={[styles.txtbox, Platform.OS == "ios"?{fontWeight: 'bold'}:{}]}
                      editable = {false}
                    ></TextInput>
                  </View>
                  
                  <View>
                    <TouchableOpacity style={styles.acceptBtn}>
                      <TextRegular content="ตกลง" style={styles.acceptTxt}/>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
          </View>
        </View>

        <View style={styles.bottom}>
          <View style={styles.bottomContent}>
            <TouchableOpacity style={styles.cancelBtn}>
              <TextRegular content="ยกเลิก" style={styles.cancelTxt}/>
            </TouchableOpacity> 
          </View>

          <View style={{flex:1}}> 

          </View>
        </View>
      </View>
      </KeyboardAwareScrollView>
      
    );
  }
} 

// Home.defaultProps = {
//   onAboutPress: noop
// };

// Home.propTypes = {
//   setTitle: PropTypes.func,
//   onAboutPress: PropTypes.func,
//   setText: PropTypes.func,
//   title: PropTypes.string,
//   saveNote: PropTypes.func,
//   notes: PropTypes.array,
//   text: PropTypes.string
// };

{/* <View style={styles.container}>
<View style={styles.viewImage}>
    <Image
        style={styles.image}
        source={{uri: 'https://www.thai-hotnews.com/tn.php?fn=https://obs.line-scdn.net/0hxgeocCstJ0hYFggFGxhYH2JAJCdrejRLPCB2SwR4eXx9JTAbMHRgJntEcHByIGAWNiVoKXkVPHlyIWcYYHVg/w644'}}
    />
</View>

<View style={styles.viewContent}>
    <TextBold content="ตรวจสอบข้อมูล" style={styles.headerText}/>

    <View>
      <TextRegular content="เลขทะเบียน" style={styles.contentTxt}/>
      <TextInput style={[styles.txtbox, Platform.OS == "ios"?{fontWeight: 'bold'}:{}]}></TextInput>
    </View>
    
    <View>
      <TextRegular content="จังหวัด" style={styles.contentTxt}/>
      <TextInput 
        style={[styles.txtbox, Platform.OS == "ios"?{fontWeight: 'bold'}:{}]}
        editable = {false}
      ></TextInput>
    </View>
    

    <View>
      <TouchableOpacity style={styles.acceptBtn}>
        <TextRegular content="ตกลง" style={styles.acceptTxt}/>
      </TouchableOpacity>

      <TouchableOpacity style={styles.cancelBtn}>
        <TextRegular content="ยกเลิก" style={styles.cancelTxt}/>
      </TouchableOpacity> 
    </View>
</View>
</View> */}
export default Validate;
