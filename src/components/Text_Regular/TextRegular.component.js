import React, {Component} from 'react';
import {Text,Platform} from 'react-native';


class TextRegular extends Component {

    constructor(props) {
        super(props);
    }

  render () {
    return (
        <Text 
                style={[this.props.style, 
                      Platform.OS == "ios"?{fontFamily:"TISCO"}:{fontFamily:"TISCO Regular"}
                    ]}>{this.props.content}
        </Text>
    );
  }
}

export default TextRegular;
