import React, {Component} from 'react';
import {Text,Platform} from 'react-native';


class TextBold extends Component {

    constructor(props) {
        super(props);
    }

  render () {
    return (
        <Text 
                style={[this.props.style, 
                      Platform.OS == "ios"?{fontFamily:"TISCO",
                                            fontWeight: 'bold'}:{fontFamily:"TISCO Bold"}
                    ]}>{this.props.content}
        </Text>
    );
  }
}

export default TextBold;
