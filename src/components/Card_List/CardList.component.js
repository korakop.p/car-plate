import React, {Component} from 'react';
import {Image,View,Text} from 'react-native';
import styles from './CardList.component.style'
import TextBold from '../Text_Bold/TextBold.component'
import TextRegular from '../Text_Regular/TextRegular.component'


class CardList extends Component {

    constructor(props) {
        super(props);
    }

  render () {
    return (
        <View style={styles.container}>
          <View style={styles.viewImage}>
            <Image 
              source={{uri: 'https://www.thai-hotnews.com/tn.php?fn=https://obs.line-scdn.net/0hxgeocCstJ0hYFggFGxhYH2JAJCdrejRLPCB2SwR4eXx9JTAbMHRgJntEcHByIGAWNiVoKXkVPHlyIWcYYHVg/w644'}}
              style={{width:'100%', height:'100%'}}
            >
            </Image>
          </View>
          <View style={styles.viewContent}>
            <View style={{
                          height:60,
                          width:"100%",
                          borderColor: '#A2A2A2',
                          borderBottomWidth:1,
                          flexDirection:'row'
                        }}
            >
              <View style={{flex:1}}>
                <TextBold style={styles.registerTxt} content={this.props.registerPlate}></TextBold>
                <TextRegular style={styles.provinceTxt} content={this.props.province}></TextRegular>
              </View>
              <View style={[{width:5},this.props.status == "done"?{backgroundColor:"#C20E1A"}:{backgroundColor:"#4FC4BE"}]}></View>
            </View>
            <View style={styles.viewDateTime}>
              <TextRegular style={styles.dateTimeTxt} content={this.props.dateTime}></TextRegular>
            </View>
          </View>
        </View>
        
    );
  }
}

export default CardList;
