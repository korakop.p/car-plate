import {StyleSheet} from 'react-native';

export default StyleSheet.create({

  container: {
    backgroundColor: '#FFFFFF',
    flexDirection:'row',
    padding:10,
    borderRadius:3
  },
  viewImage:{
    height:75,
    width:75
  },
  viewContent:{
    flex:1,
    paddingLeft:10,
    flexDirection:'column',
  },
  viewDateTime:{
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  dateTimeTxt:{
    fontSize: 12,
    color: '#A2A2A2',
  },
  registerTxt:{
    fontSize: 18,
    color: 'black'
  },
  provinceTxt:{
    fontSize: 16,
    color: 'black'
  }
});
