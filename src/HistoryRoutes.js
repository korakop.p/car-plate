import {Platform} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation';
import HistoryDoneScreen from './screens/HistoryDoneScreen';
import HistoryNewScreen from './screens/HistoryNewScreen';
import LinearGradient from 'react-native-linear-gradient';


const routesConfig = {
  tabBarOptions: {
    upperCaseLabel: false,
    showIcon: false,
    style: {
      backgroundColor: 'blue',
      fontFamily: Platform.OS === 'ios' ? "TISCO" : "TISCO Regular"
    },
    activeTintColor: "white",
    inactiveTintColor: "white",
    labelStyle: {
      fontSize: 32
    },
    tabStyle:{
      fontFamily: Platform.OS === 'ios' ? "TISCO" : "TISCO Regular"
    }
  },
  swipeEnabled: true,
  animationEnabled: true,
  tabBarPosition: 'top'
};

const GradientHeader = props => (
  <View>
      <LinearGradient
        start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#283D81', '#2074B9','#10ABDB' ]}
      >
        <Header {...props} />
      </LinearGradient>
    </View>
  )

export default createMaterialTopTabNavigator({
    HistoryNewScreen: {
    screen: HistoryNewScreen,
    navigationOptions: {
      title: 'เข้าใหม่',
      header: props => <GradientHeader {...props} />,
      headerStyle: {
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      },
    }
  },
  HistoryDoneScreen: {
    screen: HistoryDoneScreen,
    navigationOptions: {
      title: 'เสร็จสิ้น'
    }
  }
});