import React from 'react'
import { createStackNavigator, createAppContainer, TabNavigator } from "react-navigation";
import CameraScreen from './screens/CameraScreen'
import EditScreen from './screens/EditScreen'
import HistoryRoutes from './HistoryRoutes.js'
import ResultScreen from './screens/ResultScreen'
import PinScreen from './screens/PinScreen'
import { Provider } from 'react-redux'
import storeRedux from './redux/store'


const AppNavigator = createStackNavigator({
    CameraScreen: {
        screen: CameraScreen,
        navigationOptions: {
            header: null
        }
    },
    EditScreen: {
        screen: EditScreen,
        navigationOptions: {
            header: null
        }
    },
    HistoryScreen: {
        screen: HistoryRoutes,
        navigationOptions: {
            title: 'ประวัติการดำเนินงาน',
        }
    },
    ResultScreen: {
        screen: ResultScreen,
        navigationOptions: {
            header: null
        }
    },
    PinScreen: {
        screen: PinScreen,
        navigationOptions: {
            header: null
        }
    }
},{
    initialRouteName:'HistoryScreen',
    //headerMode:'none'
});

const App = createAppContainer(AppNavigator);

const AppProvideRedux = ()=>(
    <Provider store={storeRedux}>
        <App/>
    </Provider> 
  )

export default AppProvideRedux


