export const DETAIL_STAGE = {
    latitude: null,
    longitude: null,
    province: null,
    dateTime: null,
    image: null,
    licensePlate: null
}

export const RESULT_STAGE = {
    brand: null,
    model: null,
    color: null,
    tiscoNumber: null,
    contractNo: null,
    product: null,
    licenseNo: null,
    registerProvince: null
}

export const PROFILE_STAGE ={
    username: null,
    name: null,
    lastname: null,
    phone: null,
    mail: null
}

export const HISTORY_STAGE = {
    newStatus:[
        {
            image: null,
            licenseNo: null,
            registerProvince: null,
            dateTime: null,
        }
    ],
    processStatus:[
        {
            image: null,
            licenseNo: null,
            registerProvince: null,
            dateTime: null,
        }
    ],
    doneStatus:[
        {
            image: null,
            licenseNo: null,
            registerProvince: null,
            dateTime: null,
        }
    ]
}