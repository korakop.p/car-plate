import * as INIT_STATE from '../initialState'
import * as TYPES from '../actions/constance'

const result = (state = INIT_STATE.RESULT_STAGE, action) => {
  switch (action.type) {
  case TYPES.ACTION_SEARCH: {
    return {...state, 
            brand: action.payload.brand,
            model: action.payload.model,
            color: action.payload.color,
            tiscoNumber: '02-3333333',
            contractNo: action.payload.contractNo,
            product: action.payload.product,
            licenseNo: action.payload.licenseNo,
            registerProvince: action.payload.registerProvince
           };
  }
  default:
    return state;
  }
};

export default result;