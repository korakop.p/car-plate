import * as INIT_STATE from '../initialState'
import * as TYPES from '../actions/constance'

const profile = (state = INIT_STATE.HISTORY_STAGE, action) => {
  switch (action.type) {
  case TYPES.ACTION_HISTORY: {
    return {...state, 
            newStatus:action.payload.newStatus,
            processStatus:action.payload.processStatus,
            doneStatus:action.payload.doneStatus
           };
  }
  default:
    return state;
  }
};

export default profile;