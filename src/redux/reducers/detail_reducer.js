import * as INIT_STATE from '../initialState'
import * as TYPES from '../actions/constance'

const detail = (state = INIT_STATE.DETAIL_STAGE, action) => {
  switch (action.type) {
  case TYPES.ACTION_CAPTURE: {
    return {...state, 
            latitude: action.payload.latitude,
            longitude: action.payload.longitude,
            dateTime: action.payload.dateTime,
           };
  }
  case TYPES.ACTION_OCR: {
    return {...state, 
            province: action.payload.province,
            image: action.payload.image,
            licensePlate: action.payload.licensePlate
           };
  }
  case TYPES.ACTION_EDIT_LICENSEPLATE: {
    return {...state, 
            licensePlate: action.payload.licensePlate
           };
  }
  case TYPES.ACTION_EDIT_PROVINCE: {
    return {...state, 
            province: action.payload.province
           };
  }
  default:
    return state;
  }
};

export default detail;