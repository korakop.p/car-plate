import * as INIT_STATE from '../initialState'
import * as TYPES from '../actions/constance'

const profile = (state = INIT_STATE.PROFILE_STAGE, action) => {
  switch (action.type) {
  case TYPES.ACTION_AUHTEN: {
    return {...state, 
              username: action.payload.username,
              name: action.payload.name,
              lastname: action.payload.lastname,
              phone: action.payload.phone,
              mail: action.payload.mail
           };
  }
  default:
    return state;
  }
};

export default profile;