import {combineReducers} from 'redux';
import DETAIL from './detail_reducer'
import HISTORY from './history_reducer'
import PROFILE from './profile_reducer'
import RESULT from './result_reducer'

export default combineReducers({  
    HISTORY,
    PROFILE,
    RESULT,
    DETAIL
});
