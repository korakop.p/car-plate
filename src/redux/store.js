import {createStore,applyMiddleware} from 'redux'
import reduxThunk from 'redux-thunk'
import reduxLogger from 'redux-logger'
import reducers from './reducers/index'

const store = createStore(
    reducers,
    applyMiddleware(reduxThunk,reduxLogger)
)

export default store

