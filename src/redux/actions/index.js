import {createAction} from 'redux-actions';
import * as TYPE from './constance'


export const actionAuthen = createAction(TYPE.ACTION_AUHTEN);
export const actionCapture = createAction(TYPE.ACTION_CAPTURE);
export const actionHistory = createAction(TYPE.ACTION_HISTORY);
export const actionOcr = createAction(TYPE.ACTION_OCR);
export const actionSearch = createAction(TYPE.ACTION_SEARCH);

